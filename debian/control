Source: python-firehose
Section: python
Priority: optional
Maintainer: Paul Tagliamonte <paultag@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>,
           Matthias Klumpp <mak@debian.org>
# OK. DPMT is set to Uploader since I would *prefer* to know about changes
# made to this package, since this affects some cross-distro work, as well
# as a number of internal Debian projects. If a change is purely related to
# the packaging, do feel free to upload without my ack (e.g. debian/* maint
# work that doesn't affect upstream code), as a team upload. If I go
# unresponsive, please communicate the changes with upstream, and upload
# without my ack.
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               libxml2-utils,
               python3-all,
               python3-mock,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-six
Standards-Version: 4.6.2
Homepage: https://github.com/fedora-static-analysis/firehose
Vcs-Git: https://salsa.debian.org/python-team/packages/python-firehose.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-firehose
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python3-firehose
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-six,
Description: Python bindings to the Firehose static analysis format
 Static analysis data is, well, awesome! However, there's a problem of suffering
 from 10,000 different schemas, and unique ways of communicating the results.
 .
 Firehose aims to solve these issues by providing a common static analysis
 schema, with parsers for common tools, and lots of code to deal with
 Firehose output.
 .
 This package contains the Python 3 bindings to the Firehose XML schema.
